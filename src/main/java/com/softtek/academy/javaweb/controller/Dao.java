package com.softtek.academy.javaweb.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.conexion.Conexion;

public class Dao {
	private static JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public static int save(Person e) {
		System.out.println(e.getName());

		String query = "insert into student values('" + e.getName() + "','" + e.getAge() + "','" + e.getId() + "')";
		return jdbcTemplate.update(query);

	}

	public List<Person> getPersons() {

		 final List<Person> lista = new ArrayList<Person>();
		 
		jdbcTemplate.query("select * from student", new RowCallbackHandler() {
			public void processRow(ResultSet rs) throws SQLException {
				while (rs.next()) {
					Person per =new Person();
					per.setAge( rs.getString("age"));
					per.setId( rs.getInt("id"));
					per.setName( rs.getString("name"));
		        	lista.add(per);
		          
				}
			}
		});
	    	 	
	       return lista;
       
		
		
		
	}
}
